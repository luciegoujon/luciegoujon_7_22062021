# Groupomania

Projet 7 de la formation Développeur web d'Openclassrooms : Créez un réseau social d’entreprise

# Installation 

**Back**  
Exécuter `npm install` dans le répertoire `luciegoujon_7_22062021\Back`.  
Créer un fichier `.env` sur le modèle de `.env.sample`  
Ajouter les variables nécessaires pour se connecter à un serveur SQL.  
Exécuter `groupomania.sql` pour initialiser la base de données  

**Front**  
Exécuter `npm install` dans le répertoire `luciegoujon_7_22062021\groupomania-front`.

# Lancement en local

**Back**  
`luciegoujon_7_22062021\Back` : `node server.js`

**Front**  
`luciegoujon_7_22062021\groupomania-front` : `npm run serve`
