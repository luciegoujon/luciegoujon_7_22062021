const {queryDb} = require('../database');

exports.getAllPosts = (req, res, next) => {
    const sql = 'SELECT post.*, post_user.first_name AS post_first_name, post_user.last_name AS post_last_name, comment.*, comment_user.first_name AS comment_first_name, comment_user.last_name AS comment_last_name from post LEFT JOIN comment ON comment.com_parent_id = post.id LEFT JOIN user comment_user ON comment.com_user_id = comment_user.id INNER JOIN user post_user ON post.user_id = post_user.id ORDER BY date_created DESC';
    queryDb(sql, [])
        .then((result) => {
            let processedData = [];
            result.forEach(el => {
                let index = processedData.findIndex((x => x.postId === el.id));
                if (index === -1) {
                    processedData.push({
                        'postId': el.id,
                        'postUserId': el.user_id,
                        'postDateCreated': el.date_created,
                        'postDateModified': el.date_modified,
                        'postTitle': el.title,
                        'postContent': el.content,
                        'postState': el.state,
                        'postImageUrl': el.image_url,
                        'postFirstName': el.post_first_name,
                        'postLastName': el.post_last_name,
                        'comments': []
                    })
                    index = processedData.findIndex((x => x.postId === el.id));
                }
                if (el.com_id !== null) {
                    processedData[index].comments.push({
                        'commentId': el.com_id,
                        'commentParentId': el.com_parent_id,
                        'commentUserId': el.com_user_id,
                        'commentDateCreated':el.com_date_created,
                        'commentDateModified':el.com_date_modified,
                        'commentContent':el.com_content,
                        'commentState':el.com_state,
                        'commentFirstName':el.comment_first_name,
                        'commentLastName':el.comment_last_name,
                    });
                }
            });
            res.status(200).json({processedData})
        })
        .catch(error => res.status(500).json({error}))
};

exports.createPost = (req, res, next) => {
    const imageUrl = req.file ? `${req.protocol}://${req.get('host')}/images/${req.file.filename}` : null;
    const sql = 'INSERT INTO post (title, content, user_id, image_url) VALUES(?,?,?,?)';
    const params = [req.body.title, req.body.content, req.body.userId, imageUrl];
    queryDb(sql, params)
        .then(() => res.status(201).json({message: 'post created'}))
        .catch(error => res.status(500).json({error}))
}

exports.createComment = (req, res, next) => {
    const sql = 'INSERT INTO comment (com_content, com_user_id, com_parent_id) VALUES(?,?,?)';
    const params = [req.body.content, req.body.userId, req.params.id];
    queryDb(sql, params)
        .then(() => res.status(201).json({message: 'comment created'}))
        .catch(error => res.status(500).json({error}))
}

exports.modifyComment = (req, res, next) => {
    const sql = 'SELECT com_user_id FROM comment WHERE com_id=?';
    const params = [req.params.id];
    queryDb(sql, params)
        .then((result) => {
            if (result[0].com_user_id === req.body.decodedToken.userId) {
                const sqlBis = 'UPDATE comment SET com_content=?, com_date_modified=NOW() WHERE com_id=?';
                const paramsBis = [req.body.content, req.params.id];
                queryDb(sqlBis, paramsBis)
                    .then(() => res.status(200).json({message: 'comment updated'}))
                    .catch(error => res.status(500).json({error}))
            } else {
                throw 'unauthorized';
            }
        })
        .catch(error => res.status(403).json({error}))
    
}

exports.modifyPost = (req, res, next) => {
    const sql = 'SELECT user_id, image_url FROM post WHERE id=?';
    const params = [req.params.id];
    queryDb(sql, params)
        .then((result) => {
            if (result[0].user_id == req.body.decodedToken.userId) {
                const imageUrl = req.file ? `${req.protocol}://${req.get('host')}/images/${req.file.filename}` : result[0].image_url;
                const sqlBis = 'UPDATE post SET title=?, content=?, image_url=?, date_modified=NOW() WHERE id=?';
                const paramsBis = [req.body.title, req.body.content, imageUrl, req.params.id];
                queryDb(sqlBis, paramsBis)
                    .then(() => res.status(200).json({message: 'post updated'}))
                    .catch(error => res.status(500).json({error}))
            } else {
                throw 'unauthorized';
            }
        })
        .catch(error => res.status(403).json({error}))
}

exports.deletePost = (req, res, next) => {
    const sql = 'SELECT user_id FROM post WHERE id=?';
    const params = [req.params.id];
    queryDb(sql, params)
        .then((result) => {
            if (result[0].user_id == req.body.decodedToken.userId || req.body.decodedToken.role === 1 ) {
                const sqlBis = 'DELETE FROM post WHERE id=?';
                const paramsBis = [req.params.id];
                queryDb(sqlBis, paramsBis)
                    .then(() => res.status(204).json({message: 'post deleted'}))
                    .catch(error => res.status(500).json({error}))
            }
        })
        .catch(error => res.status(403).json({error}))
}

exports.deleteComment = (req, res, next) => {
    const sql = 'SELECT com_user_id FROM comment WHERE com_id=?';
    const params = [req.params.id];
    queryDb(sql, params)
        .then((result) => {
            if (result[0].com_user_id == req.body.decodedToken.userId || req.body.decodedToken.role === 1 ) {
                const sqlBis = 'DELETE FROM comment WHERE com_id=?';
                const paramsBis = [req.params.id];
                queryDb(sqlBis, paramsBis)
                    .then(() => res.status(204).json({message: 'comment deleted'}))
                    .catch(error => res.status(500).json({error}))
                }
            })
        .catch(error => res.status(403).json({error}))
}