const {queryDb} = require('../database');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.signup = (req, res, next) => {
    const sql = 'SELECT COUNT(email) AS userExisting from user WHERE email = ?';
    const params = [req.body.email];
    queryDb(sql, params)
        .then(result => {
            if (result[0].userExisting) {
                res.status(404).json({error: 'email already in use'});
                return;
            }
            bcrypt.hash(req.body.password, 10)
            .then(hash => {
                const params = [
                    req.body.firstName,
                    req.body.lastName,
                    req.body.email,
                    hash
                ];
                const sql = 'INSERT INTO user (first_name, last_name, email, password) VALUES (?, ?, ?, ?)';
                return queryDb(sql, params);
            })
            .then(response => {
                if (response) {
                    const sql = 'SELECT id, first_name, email, password, role from user WHERE email = ?';
                    const params = [req.body.email];
                    queryDb(sql, params)
                        .then(result => {
                            if (result.length > 0) {
                                bcrypt.compare(req.body.password, result[0].password)
                                    .then(valid => {
                                        if (!valid) {
                                            return res.status(401).json({ error: 'authentification failed'})
                                        }
                                        res.status(201).json({
                                            userId: result[0].id,
                                            firstName: result[0].first_name,
                                            role: result[0].role,
                                            token: jwt.sign(
                                                { userId: result[0].id,
                                                role: result[0].role},
                                                process.env.TOKEN_SECRET,
                                                { expiresIn: '24h'}
                                            )
                                        });
                                    })
                                    .catch(error => res.status(500).json({ error }))  
                            }
                        })
                        .catch(error => res.status(500).json({ error })) 
                };
            })
        })
        .catch(error => res.status(500).json({error}))
}

exports.login = (req, res, next) => {
    const sql = 'SELECT id, first_name, email, password, role from user WHERE email = ?';
    const params = [req.body.email];
    queryDb(sql, params)
        .then(result => {
            if (result.length > 0) {
                bcrypt.compare(req.body.password, result[0].password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'authentification failed'});
                    }
                    res.status(200).json({
                        userId: result[0].id,
                        firstName: result[0].first_name,
                        role: result[0].role,
                        token: jwt.sign(
                            { userId: result[0].id,
                                role: result[0].role},
                            process.env.TOKEN_SECRET,
                            { expiresIn: '24h'}
                        )
                    });
                })
                .catch(error => res.status(500).json({ error }))   
            } else {
                return res.status(401).json({ error: 'authentification failed'});
            }
        })
        .catch(error => res.status(500).json({ error }))
}

exports.deleteUser = (req, res, next) => {
    if (req.body.decodedToken.userId == req.params.id) {
        const sql = 'SELECT password from user WHERE id=?';
        const params = [req.params.id];
        queryDb(sql, params)
            .then(result => {
                bcrypt.compare(req.body.password, result[0].password)
                    .then(valid => {
                        if (!valid) {
                            return res.status(401).json({error: 'incorrect password'})
                        }
                        const sqlBis = 'DELETE FROM user WHERE id=?';
                        const paramsBis = [req.params.id];
                        queryDb(sqlBis, paramsBis)
                            .then(() => res.status(204).json({message: 'user deleted'}))
                            .catch(error => res.status(500).json({ error }))
                    })
                    .catch(error => res.status(500).json({ error })) 
            })
    } else {
        res.status(403).json({error: 'unauthorized'});
    }
}

exports.modifyUser = (req, res, next) => {
    if (req.body.decodedToken.userId == req.params.id) {
        const sql = 'SELECT password from user WHERE id=?';
        const params = [req.params.id];
        queryDb(sql, params)
            .then(result => {
                bcrypt.compare(req.body.oldPassword, result[0].password)
                    .then(valid => {
                        if (!valid) {
                            return res.status(401).json({error: 'incorrect password'})
                        }
                        bcrypt.hash(req.body.newPassword, 10)
                            .then(hash => {
                                const sqlBis = 'UPDATE user SET password=?, date_modified=NOW() WHERE id=?';
                                const paramsBis = [hash, req.params.id];
                                queryDb(sqlBis, paramsBis)
                                    .then(() => res.status(200).json({message: 'user updated'}))
                                    .catch(error => res.status(500).json({error}))
                            })
                    })
                    .catch(error => res.status(500).json({ error })) 
            })
    } else {
        res.status(403).json({error: 'unauthorized'});
    }
}

exports.getUser = (req, res, next) => {
    if (req.body.decodedToken.userId == req.params.id) {
        const sql = 'SELECT email, first_name, last_name, date_created, role FROM user WHERE id=?';
        const params = [req.params.id];
        queryDb(sql, params)
            .then((result) => res.status(200).json({result}))
            .catch(error => res.status(500).json({error}))
    } else {
        res.status(403).json({error: 'unauthorized'});
    }
}