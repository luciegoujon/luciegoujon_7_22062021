const express = require('express');
const router = express.Router();
const contentController = require('../controllers/contentCtrl');
const {auth} = require('../middleware/auth');
const multer = require('../middleware/multer');
const {checkFormPost, checkFormComment} = require('../middleware/validator');

router.post('/posts', auth, multer, checkFormPost, contentController.createPost);
router.put('/posts/:id', auth, multer, checkFormPost, contentController.modifyPost);
router.delete('/posts/:id', auth, contentController.deletePost);
router.post('/comments/:id', auth, checkFormComment, contentController.createComment);
router.put('/comments/:id', auth, checkFormComment, contentController.modifyComment);
router.delete('/comments/:id', auth, contentController.deleteComment);
router.get('/posts', auth, contentController.getAllPosts);

module.exports = router;