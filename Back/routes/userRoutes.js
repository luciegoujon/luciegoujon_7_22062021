const express = require('express');
const router = express.Router();
const userController = require('../controllers/userCtrl');
const {checkSignup} = require('../middleware/validator');
const {auth} = require('../middleware/auth');

router.post('/signup', checkSignup, userController.signup);
router.post('/login', userController.login);
router.delete('/user/:id', auth, userController.deleteUser);
router.put('/user/:id', auth, userController.modifyUser);
router.get('/user/:id', auth, userController.getUser);

module.exports = router;