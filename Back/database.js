const mysql = require('mysql');
const connection = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE
});
connection.connect(error => {
    if (error) {
        throw ('error when trying to connect to database: ' + error);
    }
});

exports.queryDb = async (sql, params) => new Promise((resolve, reject) => {
    const handler = (error, results) => {
        if (error) {
            reject('error with query: ' + error);
            return;
        }
        resolve(results);
    }
    connection.query(sql, params, handler);
});