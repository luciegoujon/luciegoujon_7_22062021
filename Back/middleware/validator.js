const {validationResult, check} = require('express-validator');

exports.checkSignup = [
    check('firstName').isLength({min: 1, max: 30}),
    check('lastName').isLength({min: 1, max: 30}),
    check('email').isEmail(),
    check('password').isLength({min: 6}),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return res.status(422).json({error: errors.array()});
        next();
    },
];

exports.checkFormPost = [
    check('title').trim().isLength({min: 2, max: 255}),
    check('content').trim().isLength({max: 5000}),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return res.status(422).json({error: errors.array()});
        next();
    },
];

exports.checkFormComment = [
    check('content').trim().isLength({min: 2, max: 3000}),
    (req, res, next) => {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return res.status(422).json({error: errors.array()});
        next();
    },
];
