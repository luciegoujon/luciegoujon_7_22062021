const jwt = require('jsonwebtoken');

exports.auth = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, process.env.TOKEN_SECRET);
        if (req.body.userId && req.body.userId !== decodedToken.userId) {
            throw 'forbidden';
        } else {
            req.body.decodedToken = decodedToken;
            next();
        }
    } catch (error) {
        res.status(401).json({error});
    }
};