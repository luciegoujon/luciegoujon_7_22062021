-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2021 at 02:17 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `groupomania`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `com_id` int(11) NOT NULL,
  `com_parent_id` int(11) NOT NULL,
  `com_user_id` int(10) UNSIGNED NOT NULL,
  `com_date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `com_date_modified` datetime DEFAULT NULL,
  `com_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`com_id`, `com_parent_id`, `com_user_id`, `com_date_created`, `com_date_modified`, `com_content`) VALUES
(1, 2, 3, '2021-09-20 16:15:48', NULL, 'Exemple de commentaire. Exemple de commentaire. Exemple de commentaire. Exemple de commentaire. Exemple de commentaire. Exemple de commentaire. Exemple de commentaire.');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime DEFAULT NULL,
  `title` tinytext NOT NULL,
  `content` text NOT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `user_id`, `date_created`, `date_modified`, `title`, `content`, `image_url`) VALUES
(1, 2, '2021-09-20 16:11:06', NULL, 'Exemple de post avec texte', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sem dolor, fringilla vel ipsum eu, malesuada lobortis justo. Suspendisse sed ullamcorper libero. Aenean finibus risus lorem, vitae facilisis est faucibus a. Curabitur eu sapien vitae nisi finibus blandit et ultricies mi. In ultrices ante id efficitur aliquam. Fusce at arcu vitae metus pellentesque vehicula. Nulla facilisis odio in sem maximus laoreet. Vivamus finibus urna vitae convallis faucibus.\n\nProin porta porttitor nisi. Integer in tortor sed eros sollicitudin consequat non eget purus. Vestibulum ultrices hendrerit accumsan. In id velit elit. Pellentesque in laoreet odio. Phasellus facilisis auctor dolor, sed molestie nibh. Phasellus luctus ante nec augue suscipit, nec hendrerit elit auctor. Mauris vehicula purus ut dolor eleifend, ultricies imperdiet odio ultrices. Donec ac aliquet eros.', NULL),
(2, 2, '2021-09-20 16:13:51', '2021-09-20 16:14:09', 'Exemple de post modifié', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sem dolor, fringilla vel ipsum eu, malesuada lobortis justo. Suspendisse sed ullamcorper libero. Aenean finibus risus lorem, vitae facilisis est faucibus a. Curabitur eu sapien vitae nisi finibus blandit et ultricies mi. In ultrices ante id efficitur aliquam. Fusce at arcu vitae metus pellentesque vehicula. Nulla facilisis odio in sem maximus laoreet.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(60) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `first_name`, `last_name`, `role`, `date_created`, `date_modified`) VALUES
(1, 'admin@groupomania.fr', '$2b$10$/oxxvjU6t2KKq6VURdf8z.TSGmhJOGFe6uooaHiSWRrOz.q2uF/Fm', 'Admin', 'Groupomania', 1, '2021-09-20 16:08:08', NULL),
(2, 'jdupont@groupomania.fr', '$2b$10$fpx8ck8FU7SCnnlGpqC7EeBpI.sWt6X/SHIo9ByyMorXdFs8E5Hlm', 'Jean', 'Dupont', 0, '2021-09-20 16:10:07', NULL),
(3, 'mmartin@groupomania.fr', '$2b$10$x8BmE9ox1tEIgsA6Jkk1d.0RkV3ATZnIuCxzvhPcmVpK40Y/bYyO6', 'Marie', 'Martin', 0, '2021-09-20 16:15:18', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `fk_parent_id` (`com_parent_id`),
  ADD KEY `fk_com_user_id` (`com_user_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_com_user_id` FOREIGN KEY (`com_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_parent_id` FOREIGN KEY (`com_parent_id`) REFERENCES `post` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
