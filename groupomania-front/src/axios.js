import axios from "axios";
axios.defaults.baseURL = 'http://localhost:3000/api';
axios.defaults.timeout = 2500;
export default axios;