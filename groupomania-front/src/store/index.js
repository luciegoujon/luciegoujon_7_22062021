import { createStore } from 'vuex';
import createPersistedState from "vuex-persistedstate";

export default createStore({
  state: {
    userId: 0,
    firstName: '',
    loggedIn: false,
    role: 0,
    token: ''
  },
  mutations: {
    LOGIN_SUCCESS(state) {
      state.loggedIn = true;
    },
    UPDATE_NAME(state, payload) {
      state.firstName = payload.firstName;
    },
    UPDATE_TOKEN(state, payload) {
      state.token = payload.token;
    },
    UPDATE_USERID(state, payload) {
      state.userId = payload.userId;
    },
    UPDATE_ROLE(state, payload) {
      state.role = payload.role;
    },
    LOGOUT(state) {
      state.userId = 0;
      state.firstName = '';
      state.loggedIn = false;
      state.role = 0;
      state.token = '';
    }
  },
  actions: {
    login({commit}, payload) {
      commit('LOGIN_SUCCESS');
      commit('UPDATE_NAME', payload);
      commit('UPDATE_TOKEN', payload);
      commit('UPDATE_USERID', payload);
      commit('UPDATE_ROLE', payload);
    },
    logout({commit}) {
      return new Promise((resolve) => {
        commit('LOGOUT');
        resolve()
      })
    }
  },
  modules: {
  },
  plugins: [createPersistedState()]

})
